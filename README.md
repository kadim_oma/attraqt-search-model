### The purpose of this test is to create a robust search model, that takes as an input summarized texts (headlines), and outputs a ranking of texts that correspond to the input.

### In order to solve this problem, I chose to proceed in two different ways:



## **Embedding of texts without topic modeling (LDA)** 


 I will follow the following steps:


1.   Creation of a preprocessing and embedding pipeline
2.   Vectorization of all the texts in our data base (using the embedding)
3.   choice of a similarity function (fast cosine similarity)

## **Embedding of texts with topic modeling (LDA)**

In this case, in addition to the steps mentioned previously, I will add a topic modeling step just before the computation of similarities between the input and the texts.
The purpose of LDA (latent Dirichlet allocation) is to exctract **semantic features** from the documents, and build similarities based on these semantic features, and not on overlapping words between texts.

### *N.B:*  
*   I will not need to split the texts database into test and training sets, as there will be no training (we will only compute the vectors of embedding of the texts)
*   The metric that I used to evaluate the search model is recall@k, for k=1, 10 then 30.
### **performances**:
#### I obtained the following recall@k report:
| k | Recall@k |
| ------ | ------ |
| 1 | 55% |
| 10 | 81% |
| 30 | 88% |




